def checkout() {
    checkout([$class: 'GitSCM', branches: [[name: '*/master']], doGenerateSubmoduleConfigurations: false, extensions: [[$class: 'CloneOption', shallow: true]], submoduleCfg: [], userRemoteConfigs: [[credentialsId: '99756240-e6a2-4ee3-a69a-e99793ada605', url: 'http://iltlvvmgitsrv:8080/git/DevOps/Intosite.git/']]])

}


def vault() {

    withEnv(["VAULT_ADDR=http://146.122.113.227:8200"]) {
        withCredentials([usernamePassword(usernameVariable:'VAULT_ROLEID',passwordVariable:'VAULT_SECRETID',credentialsId:'VAULTROLE')]){
                    AUTH = sh (returnStdout: true, script: """
                    #!/bin/bash
                    cd /tmp
                    token=\$(./vault write  auth/approle/login role_id=\${VAULT_ROLEID} secret_id=\${VAULT_SECRETID} -format=json | jq .auth.       client_token | tr --delete '"')
                    ./vault login  \$token  -format=json
                    ./vault read   project129/creds/ecs -format=json 
                    """).trim()
                } 
            
}
}
return this
